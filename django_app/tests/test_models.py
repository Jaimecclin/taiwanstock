from django.test import TestCase

def test_model1(self):
    ''' Test model 1''' 
    self.assertEqual("This is string", "This is string")

def test_model2(self):
    ''' Test model 2''' 
    self.assertEqual("This is string", "This is not string")